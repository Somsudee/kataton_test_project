<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>testSuite_logIn_failure</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ca137914-033f-4024-ab47-541687ec1709</testSuiteGuid>
   <testCaseLink>
      <guid>e5cc186f-9604-436e-bf1f-a69e1449428e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Failed</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cab58ca5-7598-43f1-834f-ac8934d53d0c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data_nameList</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>cab58ca5-7598-43f1-834f-ac8934d53d0c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>5b96175a-cdc8-41be-8f29-7fdd635ae8d9</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>

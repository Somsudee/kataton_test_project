<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Transaction List</name>
   <tag></tag>
   <elementGuidId>e9d49b3b-cbd4-43ca-83d7-91b7b0a20e4c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;add-row&quot;)/div[@class=&quot;col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='add-row']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
        Transaction List
        
        
      
      
        
          
            #
            Transaction ID
            Products
            Amount
            
          
        
        
          
            1
            1
            Garden, Papaya
            20,120 THB             
          
            2
            2
            Banana, Garden, Banana, Rambutan
            60,570 THB             
          
            3
            3
            Garden
            40,000 THB             
          
            4
            4
            Garden
            20,000 THB             
          
            5
            5
            Garden
            40,000 THB             
          
            6
            6
            Garden
            20,000 THB             
          
            7
            7
            Garden
            20,000 THB             
          
            8
            8
            Garden
            20,000 THB             
          
            9
            9
            Garden
            40,000 THB             
          
            10
            10
            Garden
            40,000 THB             
          
            11
            11
            Banana, Papaya
            162 THB             
          
            12
            12
            Garden
            20,000 THB             
          
            13
            13
            Orange
            280 THB             
          
            14
            14
            Garden
            20,000 THB             
          
            15
            15
            Garden
            20,000 THB             
          
            16
            16
            Garden
            20,000 THB             
          
            17
            17
            Garden
            20,000 THB             
          
            18
            18
            Garden
            20,000 THB             
          
            19
            19
            Garden
            20,000 THB             
          
            20
            20
            Garden
            20,000 THB             
          
            21
            21
            Garden
            20,000 THB             
          
            22
            22
            Garden
            20,000 THB             
          
            23
            23
            Garden
            20,000 THB             
          
        
      
      
        
          Total price:  541,132 THB
        
            
  </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-row&quot;)/div[@class=&quot;col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='add-row']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Transaction'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
   </webElementXpaths>
</WebElementEntity>
